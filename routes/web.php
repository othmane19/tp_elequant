<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CartController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\ProduitController;
use App\Http\Controllers\CategorieController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('home');
})->name('accueil');
Route::get("/",[HomeController::class,"index"])->name("accueil");
Route::get("/add-to-cart/{id}", [HomeController::class, "add_cart"])->name("home.add_cart");
Route::get("/cart", [CartController::class, "index"])->name("cart.index");
Route::get("/cart/detail/{id}", [CartController::class, "detail"])->name("cart.detail");
Route::get("/cart/remove/{id}", [CartController::class, "removeItem"])->name("cart.remove");
Route::get("/cart/clear", [CartController::class, "clear"])->name("cart.clear");
Route::get("/cart/submitOrder", [CartController::class, "submitOrder"])->name("cart.submitOrder");
Route::get("/cart/commander",[CartController::class,"commander"])->name("cart.commander");

Route::post("categories/searsh",[CategorieController::class,"searsh"])->name("categories.searsh");
Route::get("produits/searsh",[ProduitController::class,"searsh"])->name("produits.searsh");
Route::get("produits/null_quantite",[ProduitController::class,"null_quantite"])->name("produits.null_quantite");
Route::resource("categories",CategorieController::class);
Route::resource("produits",ProduitController::class);

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
