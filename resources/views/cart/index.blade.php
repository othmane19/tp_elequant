<!-- resources/views/cart/index.blade.php -->

@extends('layouts.admin')
@section('title', 'Shopping Cart')
@section('content')

<h1>Shopping Cart</h1>

@if (count($cartItems) > 0)



<br><br>
    <table class="table">
        <thead>
            <tr>
                <th>Product</th>
                <th>Price</th>
                <th>quantite</th>
                <th>total</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($cartItems as $item)
                <tr>
                    <td>{{ $item->designation }}</td>
                    <td>{{ $item->prix_u }}</td>
                    <td>{{ $item->quantity }}</td>
                    <td>{{ $item->prix_u * $item->quantity }}</td>
                    <td>
                        <a href="{{ route('cart.remove', $item->id) }}" class="btn btn-danger">Remove</a>
                        
                    </td>
                    
                </tr>
            @endforeach
        </tbody>
    </table>
@else
    <p>Your cart is empty.</p>
@endif

<a href="{{ route('accueil') }}" class="btn btn-primary">Continue Shopping</a>
<br><br>
<a href="{{ route("cart.clear") }}" class="btn btn-danger">clear panier</a>
<br>
<br>
<div class="d-flex flex-row-reverse me-4 " >
    <a href="{{ route('cart.commander') }}" class="btn btn-warning">commander</a>
</div>

@endsection
