@extends('layouts.admin')
@section('title', 'Shopping Cart')
@section('content')
    <div class="container">
        <form class="needs-validation"  method="GET" action="{{route("cart.submitOrder")}}">
          @csrf
            <div class="form-row">
              <div class="col-md-4 mb-3">
                <label for="validationTooltip01">Nom :</label>
                <input type="text" class="form-control" name="nom" placeholder="First name" required>
              </div>
              <div class="col-md-4 mb-3">
                <label for="validationTooltip02">Prènom :</label>
                <input type="text" class="form-control" name="prenom" placeholder="Last name" required>

              </div>
              <div class="col-md-4 mb-3">
                <label for="validationTooltipUsername">Telephone :</label>
                <div class="input-group">
                  <input type="telephone" class="form-control" name="telephone" placeholder="Telepone" required>
                </div>
              </div>
            
            <div class="form-row">
              <div class="col-md-3 mb-3">
                <label for="validationTooltip03">ville : </label>
                <input type="text" class="form-control"  name="ville" placeholder="ville" required>

              </div>
              <div class="col-md-6 mb-3">
                <label for="validationTooltip04">Adresse :</label>
                <input type="text" class="form-control"  name="adresse" placeholder="adresse" required>
              </div>
            </div>
            <input type="submit" class="btn btn-success">
          </form>
    

    <br>
    <br>
    <a href="{{ route('accueil') }}" class="btn btn-primary">Continue Shopping</a>
</div></div>
@endsection
