@extends('layouts.admin')
@section('title', 'Shopping Cart')
@section('content')
    <div class="container">
            
              <div class="col-md-3 mb-4 mx-auto">
                <div class="card">
                    <img class="card-img-top" src='{{$produit->image}}' alt="Card image cap">
                    <div class="card-body">
                        <h5 class="card-title">Produits</h5>
                        <p class="card-text">{{$produit->designation}}</p>
                        <p class="card-text"><strong> quantite stock : {{$produit->quantite_stock}} </strong></p>
                        <p class="card-text"><strong>Prix : {{$produit->prix_u}}</strong></p>
                        <br><br>
                        
                    </div>
                </div>
            
          </div>
    </div>
    <br>
    <br>
    <a href="{{ route('accueil') }}" class="btn btn-primary">Continue Shopping</a>
@endsection
