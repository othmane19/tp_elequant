 @extends('layouts.admin')
 @section('title','Modifier un produit')
 @section('content')

<div class="contain">
    <h1 class="edit-h1">Modifier le produit num {{$prod->id}}</h1>
    <form action="{{route('produits.update',["produit"=>$prod->id])}}" method="POST">
        @csrf
        @method('PUT')
        @csrf
        <div class="form-group">
        <label for="designation">Designation :</label>
        <input type="text" name="designation" id="designation" value="{{old('designation',$prod->designation)}}">
        </div>
        <br>
        <div class="form-group">
            <label for="prix">prix :</label>
            <input type="number" name="prix" id="prix" value="{{old('prix',$prod->prix_u)}}">
        </div>        
        <br>
        <div class="form-group">
            <label for="quantite_stock">quantite stock :</label>
            <input type="number" name="quantite_stock" id="quantite_stock" value="{{old('quantite_stock',$prod->quantite_stock)}}">
        </div>
        <div class="form-group">
            <label for="image">image :</label>
            <input type="file" name="image" id="image" value="{{old('image',$prod->image)}}">b
        </div>
        <br>
        
        <select class="form-select" aria-label="Default select example" name="categorie_id">
            @foreach ($categories as $item)
            @if ($item->id == $prod->categorie->id)
            <option selected value="{{$item->id}}">{{$item->designation}}</option>  
            @else
            <option value="{{$item->id}}">{{$item->designation }}</option>

            @endif
            @endforeach

          </select>
          <br>
          <br>
        <div>
            <input type="submit" value="Modifier">
        </div>
    </form>

    <div>
        @if($errors->any())
        <ul>
          @foreach($errors->all() as $er)
           <li>{{$er}}</li>
           
           @endforeach
        </ul>
         


        @endif
    </div>
</div>
@endsection
