 @extends('layouts.admin')
 @section('title','Detail d \'une categorie')
 @section('content')
    <a href="{{route('produits.index')}}">Retourner vers la liste des categories</a>
    <h1>Detail de la categorie Num {{$prod->id}}</h1>
    <div>
        <p><strong>Designation:</strong> {{$prod->designation}}</p>
        <p><strong>prix:</strong> {{$prod->prix_u}}</p>
        <p><strong>quantite stock:</strong> {{$prod->quantite_stock}}</p>
        <p><strong>categorie id:</strong> {{$prod->categorie_id}}</p>
    </div>
@endsection