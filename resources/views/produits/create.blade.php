 @extends('layouts.admin')
 @section('title','Ajouter un produit')
 @section('content')
 <div class="contain">
    <h1>Creer un nouveau produit </h1>
    <form action="{{route('produits.store')}}" method="POST" enctype="multipart/form-data">
        @csrf
        <div class="form-group">
        <label for="designation">Designation :</label>
        <input type="text" name="designation" id="designation" value="{{old('designation')}}">
        </div>
        <br>
        <div class="form-group">
            <label for="prix_u">prix :</label>
            <input type="number" name="prix_u" id="prix_u" value="{{old('prix_u')}}">
        </div>        
        <br>
        <div class="form-group">
            <input type="file" name="image" id="image" accept="images/*">
        </div>
        <div class="form-group">
            <label for="quantite_stock">quantite stock :</label>
            <input type="number" name="quantite_stock" id="quantite_stock" value="{{old('quantite_stock')}}">
        </div>
        <br>
        <div class="form-group">
            <label for="categorie_id">categorie id :</label>
            <select class="form-select" aria-label="Default select example" name="categorie_id">
                <option selected>categorie id</option>
                @foreach ($categories as $cat)
                    <option value="{{$cat->id}}">{{$cat->id}}</option>
                @endforeach
                
            
            </select>
        </div>
          <br>
        <div>
            <input class="but" type="submit" value="Ajouter">
        </div>
    </form>
</div>
    <div>
        @if($errors->any())
        <ul>
          @foreach($errors->all() as $er)
           <li>{{$er}}</li>
           
           @endforeach
        </ul>
         


        @endif
    </div>
@endsection