<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="{{asset('css/main.css')}}">
    <title>@yield('title','App store')</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css" integrity="sha384-wvfXpqpZZVQGK6TAh5t6Uq7F8KfoXXPTvUsaMigM5xNCxVVkkRGU/JlU5tF5J9iQ" crossorigin="anonymous">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
</head>
<body>
    <div class="main">
        <i class="fa-solid fa-cart-shopping"></i>
        <div class="d-flex justify-content-end align-items-center">
            <a href="{{ route('cart.index')}}" class="text-dark text-decoration-none">
                <i class="fa-solid fa-cart-shopping"></i>
                <h3 class="bg-primary">Panier</h3>
            </a>
        </div>
        @yield('content')
    </div>
    <footer>
        &copy;OFPPT 2024
    </footer>
</body>
</html>
