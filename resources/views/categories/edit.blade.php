 @extends('layouts.admin')
 @section('title','Modifier une categorie')
 @section('content')

<div class="contain">
    <h1 class="edit-h1">Modifier la categorie num {{$cat->id}}</h1>
    <form action="{{route('categories.update',["category"=>$cat->id])}}" method="POST">
        @csrf
        @method('PUT')
        
            <div class="form-group">
              <label for="exampleFormControlFile1">Designation :</label>
              <input type="text" name="designation" class="form-control" id="exampleFormControlFile1" rows="3" value="{{old('designation',$cat->designation)}}">
            </div>
        <div class="form-group">
            <label for="exampleFormControlTextarea1">Description :</label>
            <textarea class="form-control" name="description" id="exampleFormControlTextarea1" rows="3">{{old('designation',$cat->description)}}</textarea>
          </div>
        <div>
            <input type="submit" value="Modifier">
        </div>
    </form>

    <div>
        @if($errors->any())
        <ul>
          @foreach($errors->all() as $er)
           <li>{{$er}}</li>
           
           @endforeach
        </ul>
         


        @endif
    </div>
</div>
@endsection
