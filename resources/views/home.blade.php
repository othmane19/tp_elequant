@extends('layouts.admin')
@section('title','Gestion des produits')
@section('content')
    
<h1>Liste des produits</h1>
<br>

<br>
<div class="container">
    <div class="row">
        @foreach ($produits as $prod)
        <div class="col-md-3 mb-4">
            <div class="card">
                <img class="card-img-top" src='{{$prod->image}}' alt="Card image cap">
                <div class="card-body">
                    <h5 class="card-title">Produits</h5>
                    <p class="card-text">{{$prod->designation}}</p>
                    <p class="card-text"><strong>Prix : {{$prod->prix_u}}</strong></p>
                    <br>
                    <a href="{{ route('cart.detail', $prod->id) }}" class="btn btn-dark">View Details</a>
                    <br>
                    <br>
                    <form action="{{ route('home.add_cart', $prod->id) }}" method="GET">
                        <input type="number" name="quantity" placeholder="Quantité" min="1" max="{{ $prod->quantite_stock }}" required>
                        @csrf
                        <button type="submit" class="btn btn-primary">Add to panier</button>
                    </form>
                </div>
            </div>
        </div>
        @endforeach
    </div>
</div>
<div class="d-flex justify-content-center">
{{ $produits->links() }}
</div>

@endsection