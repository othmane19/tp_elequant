<?php

namespace App\Http\Controllers;

use App\Models\Produit;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *

     */
    

    /**
     * Show the application dashboard.
     *

     */
    public function index()
    {
        $produits=Produit::paginate(8);
        return view("home",compact('produits'));
    }
    public function add_cart(Request $request, $id)
    {
        $produit = Produit::find($id);
        $quantityRequested = $request->input('quantity', 1);
        $quantityAvailable = $produit->quantite_stock;

        // Check if the requested quantity is available in stock
        if ($quantityRequested <= $quantityAvailable) {
            // Add the product to the cart with the requested quantity
            $produit->quantity = $quantityRequested;
            session()->push("cart", $produit);
        } else {
            // You can handle the case where the requested quantity exceeds the available stock
            // For now, redirect back with an error message
            return redirect()->back()->with('error', 'Not enough stock available');
        }

        return redirect()->route('accueil');
    }
}
