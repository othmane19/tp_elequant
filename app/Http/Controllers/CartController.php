<?php

namespace App\Http\Controllers;

use App\Models\client;
use App\Models\Produit;
use App\Models\commandes;
use Illuminate\Http\Request;
use App\Models\DetailCommande;

class CartController extends Controller
{
    public function index()
    {
        $cartItems = session('cart', []);
        $produit=Produit::all();
        return view('cart.index', compact('cartItems',"produit"));
    }

    public function removeItem($id)
    {
        $cartItems = session('cart', []);
        $index = array_search($id, array_column($cartItems, 'id'));

        if ($index !== false) {
            unset($cartItems[$index]);
        }
        session(['cart' => array_values($cartItems)]);
        return redirect()->route('cart.index');
    }

    public function clear(){
        session()->forget("cart");
        return redirect()->route('cart.index');
    }


    public function detail($id){
        $produit=Produit::find($id);
        return view('cart.detail', compact("produit"));
    }
    public function commander(){
        return view("cart.commander");
    }

    public function submitOrder(Request $request)
    {
        $request->validate([
            'nom' => 'required',
            'prenom' => 'required',
            'telephone' => 'required',
            'ville' => 'required',
            'adresse' => 'required',
        ]);

        $nom=$request->query("nom");
        $prenom=$request->query("prenom");
        $telephone=$request->query("telephone");
        $ville=$request->query("ville");
        $adresse=$request->query("adresse");
        
        $client = new client();
        $client->nom=$nom;
        $client->prenom = $prenom ;
        $client->telephone =$telephone;
        $client->ville = $ville ;
        $client->adresse = $adresse;
        $client->save();

        $commande = new commandes();
        //$commande->total = 0;
        //$commande->etat="non";
        $commande->client_id = $client->id;
        $commande->save();
        foreach (session('cart') as $item){
            $detail = new DetailCommande();
            $detail->quantite = $item['quantity'];
            $detail->prix_u = $item['prix_u'];
            $detail->total = $item["quantity"]*$item["prix_u"];
            $detail->produit_id = $item['id'];
            $detail->commande_id = $commande->id;
            $detail->save();
            $produit = Produit::where('id','=',$item['id'])->firstOrFail();
            if ($produit->quantite_stock >= $item['quantity']){
                $produit->quantite_stock -= $item['quantity '];
                $produit->save();
                }else{
                    echo "Quantité insuffisante pour ce produit!";
                    die();
                }
                
            }
            session()->forget('cart');


        return redirect()->route('accueil')->with('success', 'Order submitted successfully!');
    }
}
