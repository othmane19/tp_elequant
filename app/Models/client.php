<?php

namespace App\Models;


use App\Models\commandes;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class client extends Model
{
    protected $fillable = ['nom', 'prenom', 'telephone', 'ville', 'adresse'];

    public function commandes()
    {
        return $this->hasMany(commandes::class);
    }
}
