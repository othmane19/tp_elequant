<?php

namespace App\Models;

use App\Models\client;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class commandes extends Model
{
    protected $fillable = ['client_id', 'quantite', 'price']; // Add other fillable fields as needed

    public function client()
    {
        return $this->belongsTo(client::class);
    }
}
